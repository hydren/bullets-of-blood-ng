import game
from match import MatchState

import pygame


def setup():
    pygame.init()
    game.display = pygame.display.set_mode((800, 600))
    pygame.display.set_caption('bullets of blood ng prototype')
    game.clock = pygame.time.Clock()
    game.current_state = MatchState()


def enter_state(state):
    state.on_enter()
    state.run()


def run_game():
    setup()
    enter_state(game.current_state)
    pygame.quit()
