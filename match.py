import game

import pygame
import math
import numpy
import datetime


class MatchState:

    def __init__(self):
        self.player_sprite = pygame.image.load("player.png")
        self.player_position_x = None
        self.player_position_y = None
        self.player_angle = None
        self.player_speed = None
        self.player_angular_speed = None
        self.bullet_sprite = pygame.image.load("bullet.png")
        self.bullet_speed = None
        self.bullets = None
        self.last_fired = None

    def on_enter(self):
        display_width, display_height = game.display.get_size()
        self.player_position_x = display_width / 2
        self.player_position_y = display_height / 2
        self.player_angle = 0
        self.player_speed = 4
        self.player_angular_speed = 0.1
        self.bullet_speed = 16
        self.bullets = []
        self.last_fired = datetime.datetime.now()

    def run(self):
        display_width, display_height = game.display.get_size()
        clear_color = (0, 0, 0)
        game_finished = False

        while not game_finished:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    game_finished = True

                print(event)
                
            keystate = pygame.key.get_pressed()

            if keystate[pygame.K_UP]:
                self.player_position_x += math.sin(self.player_angle) * self.player_speed
                self.player_position_y -= math.cos(self.player_angle) * self.player_speed

            if keystate[pygame.K_DOWN]:
                self.player_position_x -= math.sin(self.player_angle) * self.player_speed/2
                self.player_position_y += math.cos(self.player_angle) * self.player_speed/2

            player_position = (self.player_position_x, self.player_position_y)

            if keystate[pygame.K_LEFT]:
                self.player_angle -= self.player_angular_speed
            elif keystate[pygame.K_RIGHT]:
                self.player_angle += self.player_angular_speed
                
            if keystate[pygame.K_SPACE] and (datetime.datetime.now()-self.last_fired).total_seconds() > 0.25:
                self.bullets.append(dict(x=self.player_position_x, y=self.player_position_y, angle=self.player_angle))
                self.last_fired = datetime.datetime.now()
                
            while self.player_angle > 2*math.pi:
                self.player_angle -= 2*math.pi

            while self.player_angle < 0:
                self.player_angle += 2*math.pi
            
            for bullet in self.bullets:
                bullet['x'] += math.sin(bullet['angle']) * self.bullet_speed
                bullet['y'] -= math.cos(bullet['angle']) * self.bullet_speed
                    
            self.bullets = [bullet for bullet in self.bullets if (0 < bullet['x'] < display_width) and ((0 < bullet['y'] < display_height))]

            pygame.display.update()
            game.display.fill(clear_color)

            player_sprite_transformed = pygame.transform.rotate(self.player_sprite, -self.player_angle * 180 / math.pi)
            player_sprite_transformed_position = numpy.subtract(player_position, numpy.divide(player_sprite_transformed.get_size(), 2))
            game.display.blit(player_sprite_transformed, player_sprite_transformed_position)
            
            for bullet in self.bullets:
                bullet_sprite_transformed = pygame.transform.rotate(self.bullet_sprite, -bullet['angle'] * 180 / math.pi)
                bullet_sprite_transformed_position = numpy.subtract((bullet['x'], bullet['y']), numpy.divide(bullet_sprite_transformed.get_size(), 2))
                game.display.blit(bullet_sprite_transformed, bullet_sprite_transformed_position)

            game.clock.tick(60)
